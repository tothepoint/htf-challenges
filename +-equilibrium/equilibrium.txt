Code: Equilibrium
Level: easy

Our data tells us that the artifact seems to be unstable, our engineers have already managed to insert the equilibrium bolts.
From our analysis we noticed that the artifact needs equal distributed high voltage in order to stabilize.

Hacker, we need your help to stabilize it, our signal to the equilibrium bolts are disturbed by the magnetic field emitted from the artifact.
Your Hackatron3310 has already picked our signal and acts as a repeater, make sure the signal is cured and is balanced so we can stabilize the foreign object.

We have provided you with a manual to guide you, make sure you read it.
