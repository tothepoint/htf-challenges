# Challenge: Character Occurence

### Situation
Convert a sequence of characters to a sequence of their occurences.

### Problem
The way to solve this puzzle is by telling how many times the current character has occurred yet, so the first character is a 1, if you pass the same character again it gives a 2. Characters are case sensitive.  Example: 

| Text    | Result  |
|:-------:|:-------:|
| abcabad | 1112231 |  

### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| text          | string        | 

example:
```
{
    "text"= "ababac",
}
```
### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| result        |  string       |
example:
```
{
    "result": "112231"
}
```