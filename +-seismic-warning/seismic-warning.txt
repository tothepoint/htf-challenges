Code: Seismic warning
Level: Easy

Hackers!!

The seismic sensors around the artifact have picked up disturbing amount of seismographic activity. It is emitting multiple vibrations in seemingly random order. Your task, should you choose to accept it, is to analyze the readings and determine when certain types of vibrations are reaching critical power levels.
Each type of vibration was assigned a different character by our scientists on site, you will have access to a sequence of these characters, depicting these vibrations and the order in which they occurred. Your job is to return a numeric sequence representing the occurrences of each vibration type.

Scientists on site also had some technical documentation that is at your disposal here [LINK TO README REPO].
