# Challenge: Leonardo Pisano

### Situation
Code: Leonardo Pisano  
Level: hard

Our engineers pushed very hard to complete the Deep Space Optical Communications platform giving us a bidirectional 10 Gbit/s air-to-ground laser connection with the artifact.  We started receiving the first inbound data-streams, but urgently need your help decoding these messages.
In the current setup we are using our Grimaldi device to send laser beams to the artifact.  Our engineers believe we need to use laser beam diffraction technology to analyse the behaviour of the beam around the artifact.
Hacker, we need your help now, to align the signal sent with the received results of these experiments.  We will send the vectors of the laser beam to your Hackatron3310, you need to help us write the complex algorithm to find the expected
results of the diffraction so we can match it with the actual streams.

Read the specs to understand the requirements for this...

### Problem
- vulgar fraction: fraction with numerator greater then 1
- unit fraction: fraction with numerator equal to 1
- for any given vulgar fraction, return the sum of <b>distinct</b> unit fractions

### Example
```
2/3  =  1/2  +  1/6
```

##### Rules:
- the numerator and denominator are natural numbers greater than 1 ( n > 1 && d > 1 )  
- the numerator is greater than the denominator ( n > d )

### Request
| Key         | Data type  | 
|:-----------:|:----------:| 
| numerator   | BigInteger | 
| denominator | BigInteger |
##### Example:
```
{
    "numerator": 2,
    "denominator": 3
}
```

### Response
| Key                | Data type  |
|:------------------:|:----------:| 
| sumOfUnitFractions | String     |

##### Format:
n1/d1 + n2/d2 + ...

##### Example:
```
{
   "sumOfUnitFractions": "1/2 + 1/6"
}
```
