[//]: # (# Challenge: Sum of Multiples)
### Situation
You received a message from your fellow thieves. The message is a small story that does not make any sense, but you discover it can be converted into a secret code by counting the occurrences of each letter in the story. We cracked the secret code they have been using to ‘encrypt’ the text. Each special character has an effect on the current count or on the prior word or letter.
There are some special cases: 
- A period '.’ decreases all values by 1, but values can never be negative. 
- When a letter is followed by an explanation mark '!’, increase this letter’s value by 3 instead of 1.
- When a letter is followed by a question mark '?’, do not count this letter, but instead, double the letter’s current value. 
- When a word is followed by a comma ',’ increase each letter’s value of the word by 2 instead of 1.

### Problem

You should count every occurrence of each letter in the story, keeping in mind the special cases. Store these values in an array in alphabetical order. ([9, 3, 3, ...] means 'a’ occurred 9 times, 'b’ occurred 3 times, ...). So you should return an array of 26 values.

### Rules
1. Counting should be done from the left to the right. Order of occurrence matters.
2. The program should be case-insensitive.
3. Other special characters that are not mentioned above (for example " and ') should be ignored.



### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| story         | String         |

example:
```
{
     “story” : “Jimmy, a cunning thief, is twenty years old.”
}
```
### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| code          | array         |

example:
```
{
     “code” : [1, 0, 0, 0, 3, 1, 0, 1, 5, 1, 0, 0, 3, 3, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 3, 0]
}
```
