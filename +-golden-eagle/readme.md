[//]: # (# Challenge: Roman Encoder)
### Problem
The artifact is engraved with Roman letters and symbols. There's a numeric keypad on one of the artifact's side, its LCD reads 
`>encoder not found`. You need to fix the keypad in order for the keypad to work properly and to unlock the next level.

The encoder is simple, encode the numeric values of the keypad to Roman numerals.
<br>Example: `convert(88) -> "LXXXVIII"` `convert(15) -> "XV"`

##### Rules:
1. Roman numerals go from 1 to 3999 (in this challenge :p)
2. The maximum of allowed repetitions is 3. 
<br>Example: `IIII` is not allowed, neither is `MMCCCCX`
3. All the basic principles of Roman numerals apply here 
<br>Example: 4 is `IV` not `IIII`
<br>Example: 9 is `IX` not `VIIII`
<br>Example: 80 is `LXXX` not `XXC`
<br><em>more info [here](https://en.wikipedia.org/wiki/Roman_numerals)</em>


### Request

| Key                | Data type                  | 
|:------------------:|:--------------------------:| 
| input       | int | 

example:
```json
{
  "input": 15
}
```

### Response

| Key                | Data type                  | 
|:------------------:|:--------------------------:| 
| solution       | String |
example:
```json
{
   "solution": "XV"
}
```


### Tips
/

