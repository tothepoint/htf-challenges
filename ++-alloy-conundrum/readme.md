[//]: # (# Challenge: Ant Bridge)
### Situation
Code: Alloy conundrum  
Level: Medium

Hackers,

An external party has supplied our endeavor with cutting-edge fully-automated drilling bots.
These bots will be used in an attempt to breach a vault in Switzerland.

The team of bots will swap out in sequence while drilling to avoid over-heating the drill bits and breaking them.
Your job is to keep track of the bots swapping place and return us the ending position in which the bots are aligned.

### Problem
My bots are drilling the hull from left to right.

If the first one overheats, the first one stops and then next one passes him, then the next, and the next, and this keeps on going.

Each time a bot overheats, it moves to the back of the line.

What clever little things they are!

This process repeats as many times as necessary.

The bots are drilling from left-to-right in the order ```A``` then ```B``` then ```C```...

What order do they end when after the drilling.

Notes

- ```-``` = one layer breached
- ```.``` = a drill overheating
- The number of bots may differ but there are always enough bots to manage the overheating
- The drilling never starts or ends with overheating
- Bots do not cut the line!

### Example

Input
```
bots = GFEDCBA
breaches = ------------...-----------
```
Output
```
result = EDCBAGF
```

Details:

bots drilling left to right. A is drilling.
```
GFEDCBA
------------...-----------
```

The first one (A) overheats at the dot.
```
GFEDCB      A
------------...-----------
```
Then B tries to drill through the layer, but also overheats.
```
AGFEDC       B
------------...-----------
```
And then C also overheats
```
BAGFED        C
------------...-----------
```
Ant eventually D manages to breach the layer, and drills on
```
         CBAGFED
------------...-----------
```
Until they finally get to stop. Ending in this order
```
                   CBAGFED
------------...-----------
```

### Request

| Key           | Data type     |
|---------------|---------------|
| bots     		  | string 		    |
| breaches     	| string 		    |
example:
```
{
	"bots": "GFEDCBA"
	"breaches": "------------...-----------"
}
```

### Response

| Key           | Data type     |
|---------------|---------------|
| bots        	| string 	    	|
example:
```
{
   "bots" : "CBAGFED"
}
```
