# Challenge: Pandigital Sequence

### Problem
In mathematics, a pandigital number is a number in a given base, that has among its significant digits each digit used in the base at least once.
For example, 1234567890 is a pandigital number in base 10.
The challenge is to calculate a sequence of pandigital numbers, starting at a certain offset and with a specified size.

### Rules
* we are looking for positive pandigital numbers in base 10
* each digit should occur exactly once
* a number can't start with digit zero
* the offset is a natural number (long in Java, FixNum in Ruby, similar sized in other languages)
* the size is a positive natural number (int in Java, FixNum in Ruby, similar sized in other languages)
* return an empty array if nothing is found

### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| offset        | long          |
| size          | integer       | 
example:
```
{
"offset": 1000000,
"size": 3
}
```

### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| values        | array of long | 
example:
```
{
   "values": [ 1023456789, 1023456798, 1023456879 ]
}
```

### Tips
https://en.wikipedia.org/wiki/Pandigital_number