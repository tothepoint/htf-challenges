[//]: # (# Challenge: The great escape)
### Problem
* Find the fastest path from the bank to the safehouse.
* Streets appear as a grid and are equally long, but each street has its own speed limit
* Streets are identified based on their orientation: West to East and North to South, respectively.  
```
    1st WE +---10----+---30----+---70----+
           |         |         |         |
BANK------>|         |         |         |
           70        20        30       120
           |         |         |         |
    2nd WE +----50---+---0-----+---70----+
         1st NS    2nd NS    3rd NS  ^   4th NS
                                     |
                                     |
                                  SAFEHOUSE
```
* Each section has it's own speed limit, e.g. the part between the intersections (1st WE, 3rd NS) and (1st WE, 4th NS) has a speed limit of 70 mph (as shown above).  
* Given are two two-dimensional arrays, `speedsNS` and `speedsWE` containing the maximum speed at each section.
    * `speedsNS[we][ns]` holds the speed for the section of the `ns`<sup>th</sup> North-South oriented street between the `we`<sup>th</sup> and the `we+1`<sup>th</sup> West-East oriented streets  
      For the example above, `speedsNS[1][3]=40` (assuming 1-based indexing)  
    * `speedsWE[we][ns]` holds the speed for the section of the `we`<sup>th</sup> North-South oriented street between the `ns`<sup>th</sup> and the `ns+1`<sup>th</sup> West-East oriented streets  
* Also, you receive the coordinates of the bank and the safehouse, which always lie exactly in the middle of a section
    * These are represented as coordinates; a coordinate contains two doubles `thStreetNorthSouth` and `thStreetNorthSouth`. These values can be of two formats: `i.0` and `i.5`. The latter represents
    the intersections in between wich the coordinate lies.
    * For example, the coordinates of the safehouse for the example above, are `(ns:3.5, we:2.0)`, i.e. on the 2nd street oriented West-East in between the 3rd and 4th streets North-South.
      
* Find the fastest path between the bank and the safehouse. The resulting sequence passed in the response should be formatted as follows: 
```
(a, b)--(c,d)--...
```
here, `(a, b)` represents an intersection neighboring the bank's street, `(c, d)` the next intersection in the sequence, etc.
### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| bankCoordinate.thStreetNorthSouth     |    double     | 
| bankCoordinate.thStreetWestEast     |    double     | 
| safeHouseCoordinate.thStreetNorthSouth     |    double     | 
| safeHouseCoordinate.thStreetWestEast     |    double     | 
| speedsNorthSouth     | int[][]        |
| speedsWestEast | int[][]|

example:
```
{
    "bankCoordinate":
        {
            "thStreetNorthSouth":1.0,
            "thStreetWestEast":1.5
        },
    "safeHouseCoordinate":
        {
            "thStreetNorthSouth":3.5,
            "thStreetWestEast":2.0
        },
    "speedsNorthSouth":[[70,20,30,120]],
    "speedsWestEast":[[10,30,70],[50,0,70]]
}
```

### Response


| Key           | Data type     | 
|:-------------:|:-------------:| 
| sequence     | String        | 

example:

```
{
    "sequence" : "(2,1)--(2,2)--(1,2)--(1,3)--(1,4)--(2,4)"
}

```

### Tips
 * Convert the speed to a measure capable of expressing the time taken to travel a certain distance. Recall that each section is equally long.
 * The speeds of the section in which bank & safehouse lie, do not matter, as entering/leaving the section in either direction results in the same traveling time, since bank & safehouse lie halfway.
