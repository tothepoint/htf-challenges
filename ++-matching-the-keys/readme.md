[//]: # (# Challenge: Matching the keys)
### Situation
We are trying to access a large safe. The content of the safe must be valuable, because the safe is protected with a lot of different keyholes. We already found all the keys, but we don’t know which key is used for which keyhole. We are unable to test all of the different combinations, because we have a limited amount of time as the alarm will go off when we enter the room where the safe is stored. Luckily we already have a blueprint of the keys and keyholes.

### Problem
Match every key with the right keyhole. Return an array where all the keys are ordered from left to right. The first key should be labeled by an integer 0.

### Rules
1. Both locks and the keys are ordered in a grid, each column is 50 x 50 pixels in size.
2. Each key can only be used once.
3. Every pixel of the key should be able to fit in a keyhole.
4. The keys and keyholes are positioned in the exact same position in the grid. (There’s no offset in pixels within the grid)

### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| keyholes      | string        | 
| keys          | string        |

example:
```
{
     "keyholes": "iVBORw0KGgoAAAANSUhEUgAAAMgAAAAyCAIAAACWMwO2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAJOgAACToAYJjBRwAAAIPSURBVHhe7dJBjuMwDATA+f+ndy91GKAxgByBkhF0XTtk07F/qqqqqqqqqqqqql7p3zaLqn7zdWywqOo3X8cGi+oNvJNlxgYo2GDRF/Og26ybpGmZsQEKNlj0xTzoNusmaVpmbICCDRZ9MQ+6zbpJmpYZG6Bgg0VfzINus26SpmXGBijYYNE++8JKOkrTNusmaVpmbJKmID5AYVhJR2naZt0kTcuMTdIUxAcoDCvpKE3brJukaZmxSZqC+ACFYSUdpWmbdZM0BfENLgjiAxQ+ZPhNXBbEkzQF8Q0uCOIDFD5k+E1cFsSTNAXxDS4I4gMUPmT4TVwWxJM0BfENLgjiAxQ+ZPhNXBbEkzQF8Q0uCOLDlP/Bj97KlUE8SVMQ3+CCID5M+R/86K1cGcSTNAXxDS4I4gMUPmT4TVwWxJM0BfENLgjiAxQ+ZPhNXBbEkzQF8Q0uCOIDFD5k+AYXLDM2SVMQ3+CCID5A4UOGb3DBMmOTNAXxDS4I4gMUPmT4BhcsM7bBogEKBigI4gMUhpX0ChcsM7bBogEKBigI4gMUhpX0ChcsM7bBogEKBigI4gMUhpX0ChcsM7bBogEKBigI4kr+oWXGNlg0QMEABUFcyT+0zNgGiwYoGKAgiOsNvJMBCgYoCOJ6A+9kgIIBCoK46jO+oyCu+ozvKIirPuM7CuKqqqqqqqqqqqqqeurn5z83lqFOLZMQFQAAAABJRU5ErkJggg==",
     "keys": "iVBORw0KGgoAAAANSUhEUgAAAMgAAAAyCAIAAACWMwO2AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAJOgAACToAYJjBRwAAAIESURBVHhe7dJBctwwDARA///TziF9Q6lEkQQ30U5fxwBE7/z8RjRIsaJFihUtUqxokWJFixQrWqRY0SLFihYzxfpZZlG8V4oVLVKsaJFifTs/yTBjd/77Ytm4zLrv4/3DjN1JsbDu+3j/MGN3Uiys+z7eP8zYnRQL676P9w8zdmfpH+pUIT7CyWXW7WBjMZKe5/wwY3eW3uNUIT7CyWXW7WBjMZKe5/wwY3eW3uNUIT7CyWXW7WBjMZKe5/wwY3eW3uNUIT7CyWXW7WBjMZKe53whnrU07xMK8T/JJxbiHWx8yPBxzhfiWSkW4h1sfMjwcc4X4lkpFuIdbHzI8HHOF+JZKRbiHWx8yPBxzhfiWSkW4t1sv+CPPsqnFOJZKRbi3Wy/4I8+yqcU4lkpFuIdbHzI8HHOF+JZKRbiHWx8yPBxzhfiWa8tlk8ZZmwHGx8yfJzzhXhWioWxHWx8yPBxzhfiWSkWxnaw8SHDu9newIELKRbGdrCxGEm3s72BAxeW3uNCIf4onzLM2A42FiPpdrY3cODC0ntcKMQf5VOGGdvBxmIk3c72Bg5cWHqPC4X4o3zKMGOv43kNHLiQYmHsdTyvgQMXXlus+MtP0sCBCynWy/lJGjhwIcWKFilWtEixokVKEC1SrGiRYkWLFCtapFjRIsWKFilWtEixokWKFQ1+f/8AZQ8gUMpU9F0AAAAASUVORK5CYII="
}
```
### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| keys          | array         |
example:
```
{
    "keys": [3, 1, 0, 2]
}
```

### Tips
1. The image of the keyholes and the keys is encoded in base64. You can view the images by copy pasting the string inside the address bar of your browser. However, you should prepend the string with `data:image/png;base64,`