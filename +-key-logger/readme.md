# Challenge: Longest matching sequence
### Situation
Code: Key logger   
Level: easy

Months ago we planted keyloggers on two computers inside the bank. 
We've collected all the key strokes and suspect that both should contain the master passwords for several high level bank accounts.  
Your job is the match the longest matching sequence from both samples. This surely must be the password we're looking for. 


### Problem

Find the longest subsequence that is present in `strandA` and `strandB`.

##### Rules:
- Position in the `String doesn't matter and when multiple sequences are tied for longest.


### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| strandA     | String| 
| strandB     | String        |
example:
```
{
    "strandA": "kjldmfqsjdlmsqjaaaafkldsmqkfjldsmqfopifpezîorrlmdfkùfdjksqmf",
    "strandB": "kjmlsdqfkldmsqfidosqfijezkfldsmqfeoprzpodsklqmfkdjsqmlfjdlsqmfjdiosqfezfiaaaa"
}
```

### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| value         | String        |
example:
```
{
   "value": "aaaa"
}
```


### Tips

- Any matching substring counts, at any location
- In AI, a sequence often gets divided into tokens. Consider tokens of ever decreasing length to find the longest match.
