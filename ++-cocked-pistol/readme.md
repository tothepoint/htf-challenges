[//]: # (# Challenge: Morse code)
### Situation
Code: Cocked Pistol  
Level: medium

A team got wireless access to a printer on the banks main floor. 
They replaced the software and made a subtle change. Now the printer wil send out all characters it prints using a low power signal. It does this by running a current through a long power line and can only vary the power between full and none. 
The printer now outputs all printed characters as a morse code signal.  
Your task is to write a program that will receive these transmissions and immediately translate the morse code back to characters. 

### Problem
* You get a long string of 0 and 1
* You have to translate this to morse code first and then turn it into letters to produce a word/sentence

##### Rules
* "Dot" is 1 time unit long.
* "Dash" is 3 time units long.
* Pause between dots and dashes in a character is 1 time unit long.
* Pause between characters inside a word is 3 time units long.
* Pause between words is 7 time units long.

### Request

| Key           | Data type     | 
|:-------------:|:-------------:| 
| value     | string        | 
example:
```
{
"value": "1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011",
}
```

### Response

| Key           | Data type     |
|:-------------:|:-------------:| 
| value     | string        | 
example:
```
{
   "value": "HEY JUDE"
}
```


### Tips
http://www.codewars.com/kata/decode-the-morse-code-advanced
